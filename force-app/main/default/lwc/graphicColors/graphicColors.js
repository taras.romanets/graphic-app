import { LightningElement, track } from 'lwc';

export default class GraphicColors extends LightningElement {
    @track RGB = {R: '', G: '', B: ''}
    @track CMYK = {C: '', M: '', Y: '', K: ''}
    @track HSL = {H: '', S: '', L: ''}
    @track lightness = 0
    isModalOpen = false
    newFileWasUploaded = false
    uploadedFileUrl = ''

    handleInputChange() {
        let canvasOriginal = this.template.querySelector("canvas[data-id=canvasOriginal]");
        let canvasChanged = this.template.querySelector("canvas[data-id=canvasChanged]");
        const ctxOriginal = canvasOriginal.getContext("2d");
        const ctxChanged = canvasChanged.getContext("2d");

        const uploader = this.template.querySelector('lightning-input');
        let imageFile = uploader.files[0];

        const img = new Image();
        img.src = URL.createObjectURL(imageFile);
        img.onload = function() {
            canvasOriginal.width = img.width
            canvasOriginal.height = img.height
            ctxOriginal.clearRect(0, 0, canvasOriginal.width, canvasOriginal.height);
            ctxOriginal.drawImage(img, 0, 0, canvasOriginal.width, canvasOriginal.height);

            canvasChanged.width = img.width
            canvasChanged.height = img.height
            ctxChanged.clearRect(0, 0, canvasChanged.width, canvasChanged.height);
            ctxChanged.drawImage(img, 0, 0, canvasChanged.width, canvasChanged.height);
        }
    }

    handleModal() {
        this.isModalOpen = !this.isModalOpen;
    }

    mousePosition(canvas, event) {
        const rect = canvas.getBoundingClientRect();

        return {
            x: Math.round(event.clientX - rect.left),
            y: Math.round(event.clientY - rect.top)
        };
    }

    handleCanvasMouseMove(e) {
        try {
            let canvasId = e.currentTarget.dataset.id
            const canvas = this.template.querySelector("canvas[data-id=" + canvasId + "]");
            const ctx = canvas.getContext("2d");
            const mouse = this.mousePosition(canvas, e)
            let pixelData = ctx.getImageData(mouse.x, mouse.y, 1, 1).data; 

            this.RGB = {R: pixelData[0], G: pixelData[1], B: pixelData[2]}
            // console.log('RGB ', this.RGB.R, this.RGB.G, this.RGB.B, ' at ', 'X: ', mouse.x, ' Y: ', mouse.y);

            this.CMYK = this.RGBtoCMYK(this.RGB.R, this.RGB.G, this.RGB.B)
            this.HSL = this.RGBtoHSL(this.RGB.R, this.RGB.G, this.RGB.B)
        } catch(error) {
            console.log(error.message);
        }
    }

    RGBtoHSL(r, g, b) {
        r /= 255;
        g /= 255;
        b /= 255;
        const l = Math.max(r, g, b);
        const s = l - Math.min(r, g, b);
        const h = s ? l === r
            ? (g - b) / s
            : l === g
            ? 2 + (b - r) / s
            : 4 + (r - g) / s
            : 0;
        return {
          H: Math.round(60 * h < 0 ? 60 * h + 360 : 60 * h),
          S: Math.round(100 * (s ? (l <= 0.5 ? s / (2 * l - s) : s / (2 - (2 * l - s))) : 0)),
          L: Math.round((100 * (2 * l - s)) / 2),
        };
    }

    HSLtoRGB(h, s, l){
        s /= 100;
        l /= 100;
        const k = n => (n + h / 30) % 12;
        const a = s * Math.min(l, 1 - l);
        const f = n => l - a * Math.max(-1, Math.min(k(n) - 3, Math.min(9 - k(n), 1)));
        return {
            R: 255 * f(0), 
            G: 255 * f(8), 
            B: 255 * f(4)
        };
    }

    RGBtoCMYK(r, g, b) {
        var computedC = 0;
        var computedM = 0;
        var computedY = 0;
        var computedK = 0;

        if (r == null || g == null || b == null || isNaN(r) || isNaN(g)|| isNaN(b) ) {
            alert ('RGB must be numeric values!');
            return;
        }
        if (r < 0 || g < 0 || b < 0 || r > 255 || g > 255 || b > 255) {
            alert ('RGB values must be in the range 0 to 255.');
            return;
        }

        if (r == 0 && g == 0 && b == 0) {
            computedK = 1;
            return { 
                C: 0, 
                M: 0,
                Y: 0, 
                K: 100
            };
        }

        computedC = 1 - (r/255);
        computedM = 1 - (g/255);
        computedY = 1 - (b/255);

        var minCMY = Math.min(computedC, Math.min(computedM,computedY));
        computedC = (computedC - minCMY) / (1 - minCMY) ;
        computedM = (computedM - minCMY) / (1 - minCMY) ;
        computedY = (computedY - minCMY) / (1 - minCMY) ;
        computedK = minCMY;

        return { 
            C: Math.floor(computedC * 100), 
            M: Math.floor(computedM * 100),
            Y: Math.floor(computedY * 100), 
            K: Math.floor(computedK * 100)
        };
    }

    downloadImage() {
        const canvas = this.template.querySelector('canvas[data-id=canvasChanged]');
        let img = document.createElement("a");
        img.href = canvas.toDataURL("image/png");
        img.download = "image.png";
        img.click();
    }

    handleSliderChange(e) {
        try {
            this.lightness = e.detail.value;
            let canvas = this.template.querySelector('canvas[data-id=canvasChanged]')
            let ctx = canvas.getContext("2d")
            let img = ctx.getImageData(0, 0, canvas.width, canvas.height)
            
            for(let i = 0; i < img.data.length; i += 4) {
                let hslPixel = this.RGBtoHSL(img.data[i], img.data[i+1], img.data[i+2]);

                if(hslPixel.H === 0 && hslPixel.S === 0 && hslPixel.L > 0 && hslPixel.L < 100) {
                    hslPixel.L = this.lightness
                }

                let rgbPixel = this.HSLtoRGB(hslPixel.H, hslPixel.S, hslPixel.L);
                img.data[i] = rgbPixel.R;
                img.data[i+1] = rgbPixel.G;
                img.data[i+2] = rgbPixel.B;
            }
            ctx.putImageData(img, 0, 0);
        } catch(error) {
            console.log(error.message);
        }
    }
}
import { LightningElement, track } from 'lwc';

export default class GraphicTransform extends LightningElement {
    //! щоб побудувати квадрат треба просто знати ліву верхню вершину і сторону
    @track grid_size = 35
    @track x_axis_distance_grid_lines = 10
    @track y_axis_distance_grid_lines = 10
    @track canvas
    @track ctx
    @track canvas_width
    @track canvas_height
    @track num_lines_x = 0;
    @track num_lines_y = 0;

    @track angleOfRotation = 0
    @track scale = 1
    @track verticalMovementSide = 'Up'
    renderCanvas = true
    side = 0

    @track vertices = {
        A: { x: 0, y: 0 },
        B: { x: 0, y: 0 },
        C: { x: 0, y: 0 },
        D: { x: 0, y: 0 },
    }

    // @track vertices_matrix = [
    //     [0, 0],
    //     [0, 0],
    //     [0, 0],
    //     [0, 0],
    // ]

    isModalOpen = false

    handleModal() {
        this.isModalOpen = !this.isModalOpen;
    }

    handleAngleSliderChange(event) {
        this.angleOfRotation = event.target.value
    }

    get verticalMovementOptions() {
        return [
            { label: 'Up', value: 'Up' },
            { label: 'Down', value: 'Down' },
        ];
    }

    handleVerticalMovementSideChange(event) {
        this.verticalMovementSide = event.detail.value
    }

    handleXInputsChange(event) {
        this.vertices[event.target.name].x = event.detail.value
        this.calculateVertices()
    }

    handleYInputsChange(event) {
        this.vertices[event.target.name].y = event.detail.value
        this.calculateVertices()
    }

    handleScaleChange(event) {
        this.scale = event.detail.value
    }

    calculateVertices() {
        this.vertices.B.x = this.vertices.A.x
        const side =  Math.sqrt(
            Math.pow(parseInt(this.vertices.B.x) - parseInt(this.vertices.A.x), 2) + 
            Math.pow(parseInt(this.vertices.B.y) - parseInt(this.vertices.A.y), 2)
        )
        this.side = side

        this.vertices.C.x = Math.round(parseInt(this.vertices.B.x) + side)
        this.vertices.C.y = this.vertices.B.y
        this.vertices.D.x = Math.round(parseInt(this.vertices.A.x) + side)
        this.vertices.D.y = this.vertices.A.y
    }

    handleDrawClick() {
        for(let i = 0; i <= this.angleOfRotation; i+=1) {
            setTimeout(() => {
                    this.setOriginToLeftTopCorner()
                    this.ctx.clearRect(0, 0, this.canvas_width, this.canvas_height)
                    this.drawCoordinatesSystem()
                    
                    let newXY = this.setOriginToSquareCenter()
                    this.ctx.rotate(Math.PI / 180 * -i);
    
                    this.ctx.fillRect(
                        -this.side/2 * this.grid_size / this.scale,
                        -this.side/2 * this.grid_size / this.scale,
                        this.grid_size / this.scale * this.side,
                        this.grid_size / this.scale * this.side
                        );
                    this.ctx.rotate(Math.PI / 180 * i);
                    this.ctx.stroke()
                    switch(newXY[2]) { //* quater
                        case 1:
                            this.ctx.translate(-newXY[0], +newXY[1])
                            break;
                        case 2:
                            this.ctx.translate(newXY[0], newXY[1])
                            break;
                        case 3:
                            this.ctx.translate(newXY[0], -newXY[1])
                            break;
                        case 4:
                            this.ctx.translate(-newXY[0], -newXY[1])
                            break;
                    }
            }, 50 * i);
        }
    }

    nextDraw() {
        this.setOriginToSquareCenter()
        this.ctx.rotate(Math.PI / 180 * -this.angleOfRotation);

        if(this.vertices.A.y > this.vertices.B.y) {
            this.ctx.fillRect(
                this.vertices.A.x * this.grid_size, 
                -this.vertices.A.y * this.grid_size, 
                this.grid_size, 
                this.grid_size);
        } else {
            this.ctx.fillRect(
                this.vertices.B.x * this.grid_size, 
                -this.vertices.B.y * this.grid_size, 
                this.grid_size, 
                this.grid_size);
        }
        this.ctx.stroke();
    }

    handleDownloadClick() {
        let img = document.createElement("a");
        img.href = this.canvas.toDataURL("image/png");
        img.download = "image.png";
        img.click();
    }

    renderedCallback() {
        if(this.renderCanvas === false) return;

        this.canvas = this.template.querySelector('canvas');
        this.ctx = this.canvas.getContext('2d');
        this.canvas_width = this.canvas.width;
        this.canvas_height = this.canvas.height;
        this.num_lines_x = Math.floor(this.canvas_height / this.grid_size);
        this.num_lines_y = Math.floor(this.canvas_width / this.grid_size);

        this.drawCoordinatesSystem()
        this.renderCanvas = false
    }

    drawCoordinatesSystem() {
        this.linesX();
        this.linesY();
        this.setOriginToCoordinatesStart();
        this.marksX();
        this.marksY();
    }
    
    linesX() {
        // Draw grid lines along X-axis
        for(var i = 0; i <= this.num_lines_x; i++) {
            this.ctx.beginPath();
            this.ctx.lineWidth = 1;

            // If line represents X-axis draw in different color
            if(i == this.x_axis_distance_grid_lines) 
            this.ctx.strokeStyle = "#000000";
            else
            this.ctx.strokeStyle = "#e9e9e9";

            if(i == this.num_lines_x) {
                this.ctx.moveTo(0, this.grid_size*i);
                this.ctx.lineTo(this.canvas_width, this.grid_size*i);
            }
            else {
                this.ctx.moveTo(0, this.grid_size*i+0.5);
                this.ctx.lineTo(this.canvas_width, this.grid_size*i+0.5);
            }
            this.ctx.stroke();
        }
    }

    linesY() {
        // Draw grid lines along Y-axis
        for(let i = 0; i <= this.num_lines_y; i++) {
            this.ctx.beginPath();
            this.ctx.lineWidth = 1;

            // If line represents X-axis draw in different color
            if(i == this.y_axis_distance_grid_lines) 
            this.ctx.strokeStyle = "#000000";
            else
            this.ctx.strokeStyle = "#e9e9e9";

            if(i == this.num_lines_y) {
                this.ctx.moveTo(this.grid_size*i, 0);
                this.ctx.lineTo(this.grid_size*i, this.canvas_height);
            }
            else {
                this.ctx.moveTo(this.grid_size*i+0.5, 0);
                this.ctx.lineTo(this.grid_size*i+0.5, this.canvas_height);
            }
            this.ctx.stroke();
        }
    }

    setOriginToSquareCenter() {
        let x = 0
        let y = 0
        let topLeftVertice = parseInt(this.vertices.A.y) > parseInt(this.vertices.B.y) ? 'A' : 'B'
        let quater = 0

        if(this.vertices[topLeftVertice].x > 0 && this.vertices[topLeftVertice].y < 0) {
            x += this.vertices[topLeftVertice].x * this.grid_size / this.scale
            y += Math.abs(this.vertices[topLeftVertice].y) * this.grid_size / this.scale
            quater = 4
        } else if(this.vertices[topLeftVertice].x > 0 && this.vertices[topLeftVertice].y > 0){
            x += this.vertices[topLeftVertice].x * this.grid_size / this.scale
            y -= this.vertices[topLeftVertice].y * this.grid_size / this.scale
            quater = 1
        } else if(this.vertices[topLeftVertice].x < 0 && this.vertices[topLeftVertice].y > 0){
            x -= Math.abs(this.vertices[topLeftVertice].x) * this.grid_size / this.scale
            y -= this.vertices[topLeftVertice].y * this.grid_size / this.scale
            quater = 2
        } else if(this.vertices[topLeftVertice].x < 0 && this.vertices[topLeftVertice].y < 0){
            x -= Math.abs(this.vertices[topLeftVertice].x) * this.grid_size / this.scale
            y += Math.abs(this.vertices[topLeftVertice].y) * this.grid_size / this.scale
            quater = 3
        }
        x += this.side * this.grid_size / this.scale / 2
        y += this.side * this.grid_size / this.scale / 2

        this.ctx.translate(x, y);
        return [Math.abs(x), Math.abs(y), quater];
    }

    setOriginToCoordinatesStart() {
        // Translate to the new origin. Now Y-axis of the canvas is opposite to the Y-axis of the graph. So the y-coordinate of each element will be negative of the actual
        this.ctx.translate(this.y_axis_distance_grid_lines * this.grid_size, this.x_axis_distance_grid_lines * this.grid_size);
    }

    setOriginToLeftTopCorner() {
        this.ctx.translate(-this.y_axis_distance_grid_lines * this.grid_size, -this.x_axis_distance_grid_lines * this.grid_size);
    }

    marksX() {
        // Ticks marks along the positive X-axis
        for(let i = 1; i < (this.num_lines_y - this.y_axis_distance_grid_lines); i++) {
            this.ctx.beginPath();
            this.ctx.lineWidth = 1;
            this.ctx.strokeStyle = "#000000";        

            // Draw a tick mark 6px long (-3 to 3)
            this.ctx.moveTo(this.grid_size*i+0.5, -3);
            this.ctx.lineTo(this.grid_size*i+0.5, 3);
            this.ctx.stroke();       

            // Text value at that point
            this.ctx.font = '9px Arial';
            this.ctx.textAlign = 'start';
            this.ctx.fillText(this.scale*i, this.grid_size*i-2, 15);
        }       

        // Ticks marks along the negative X-axis
        for(let i = 1; i < this.y_axis_distance_grid_lines; i++) {
            this.ctx.beginPath();
            this.ctx.lineWidth = 1;
            this.ctx.strokeStyle = "#000000";        

            // Draw a tick mark 6px long (-3 to 3)
            this.ctx.moveTo(-this.grid_size*i+0.5, -3);
            this.ctx.lineTo(-this.grid_size*i+0.5, 3);
            this.ctx.stroke();       

            // Text value at that point
            this.ctx.font = '9px Arial';
            this.ctx.textAlign = 'end';
            this.ctx.fillText(-this.scale*i, -this.grid_size*i+3, 15);
        }
    }
    
    marksY() {
        // Ticks marks along the positive Y-axis
        // Positive Y-axis of graph is negative Y-axis of the canvas
        for(let i = 1; i < (this.num_lines_x - this.x_axis_distance_grid_lines); i++) {
            this.ctx.beginPath();
            this.ctx.lineWidth = 1;
            this.ctx.strokeStyle = "#000000";
        
            // Draw a tick mark 6px long (-3 to 3)
            this.ctx.moveTo(-3, this.grid_size*i+0.5);
            this.ctx.lineTo(3, this.grid_size*i+0.5);
            this.ctx.stroke();
        
            // Text value at that point
            this.ctx.font = '9px Arial';
            this.ctx.textAlign = 'start';
            this.ctx.fillText(-this.scale*i, 8, this.grid_size*i+3);
        }

        // Ticks marks along the negative Y-axis
        // Negative Y-axis of graph is positive Y-axis of the canvas
        for(let i = 1; i < this.x_axis_distance_grid_lines; i++) {
            this.ctx.beginPath();
            this.ctx.lineWidth = 1;
            this.ctx.strokeStyle = "#000000";
        
            // Draw a tick mark 6px long (-3 to 3)
            this.ctx.moveTo(-3, -this.grid_size*i+0.5);
            this.ctx.lineTo(3, -this.grid_size*i+0.5);
            this.ctx.stroke();
        
            // Text value at that point
            this.ctx.font = '9px Arial';
            this.ctx.textAlign = 'start';
            this.ctx.fillText(this.scale*i, 8, -this.grid_size*i+3);
        }
    }
}
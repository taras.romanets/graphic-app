import { LightningElement, track } from 'lwc';

const MAX_ITERATION = 10

export default class GraphicFractal extends LightningElement {

    @track color = '#000'
    @track isLoaded = true
    @track scale = 1
    @track colors = []
    isModalOpen = false
    a
    b

    handleModal() {
        this.isModalOpen = !this.isModalOpen;
    }

    handleNumberAInput(e) {
        this.a = e.target.value;
    }
    handleNumberBInput(e) {
        this.b = e.target.value;
    }

    handleColorChange(e) {
        this.color = e.target.value;
    }

    handleZoomOut() {
        this.scale *= 0.8
        this.drawFractal(window.innerWidth / 2, window.innerHeight / 2, this.scale);

    }
    handleZoomIn() {
        this.scale *= 1.2
        this.drawFractal(window.innerWidth / 2, window.innerHeight / 2, this.scale);
    }

    handleDownloadClick() {
        let canvas = this.template.querySelector('canvas');
        let img = document.createElement("a");
        img.href = canvas.toDataURL("Mandelbrot-Set/png");
        img.download = "Mandelbrot-Set.png";
        img.click();
    }

    mandelbrot(c) {
        let z = { x: 0, y: 0 }, n = 0, p, d;
        do {
            p = {
                x: Math.pow(z.x, 2) - Math.pow(z.y, 2),
                y: 2 * z.x * z.y
            }
            z = {
                x: p.x + c.x,
                y: p.y + c.y
            }
            d = Math.sqrt(Math.pow(z.x, 2) + Math.pow(z.y, 2))
            n += 1
        } while (d <= 2 && n < MAX_ITERATION)
        return [n, d <= 2]
    }

    drawDefault() {
        if(this.a === undefined) {
            this.a = -2
            this.template.querySelector('lightning-input').value = this.a
        }
        if(this.b === undefined) {
            this.b = 1
            this.template.querySelectorAll('lightning-input')[1].value = this.b
        }

        this.scale = 1
        this.colors = new Array(5).fill(0).map((_, i) => `#${((1 << 24) * Math.random() | 0).toString(16)}`)
        this.drawFractal(window.innerWidth / 2, window.innerHeight / 2, this.scale);
    }

    drawFractal(width, height, scale) {
        this.isLoaded = false
        try {
            let canvas = this.template.querySelector('canvas');
            let ctx = canvas.getContext('2d');
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            const WIDTH = width
            const HEIGHT = height
            ctx.canvas.width = WIDTH
            ctx.canvas.height = HEIGHT
            ctx.scale(scale, scale)
            ctx.setTransform(scale, 0, 0, scale, 0, 0)

            const REAL_SET = { start: parseInt(this.a), end: parseInt(this.a / -2) }
            const IMAGINARY_SET = { start: parseInt(this.b), end: parseInt(this.b / -1) }

            const colors = this.colors

            for (let i = 0; i < WIDTH; i++) {
                for (let j = 0; j < HEIGHT; j++) {
                    let complex = {
                        x: REAL_SET.start + (i / WIDTH) * (REAL_SET.end - REAL_SET.start),
                        y: IMAGINARY_SET.start + (j / HEIGHT) * (IMAGINARY_SET.end - IMAGINARY_SET.start)
                    }
                
                    const [m, isMandelbrotSet] = this.mandelbrot(complex)
                    ctx.fillStyle = isMandelbrotSet ? this.color : colors[(m % colors.length - 1) + 1]
                    ctx.fillRect(i, j, 1, 1)
                }
            }
            this.isLoaded = true
        }
        catch (e) {
            console.log('Error: ' + e.message);
        }
    }
}
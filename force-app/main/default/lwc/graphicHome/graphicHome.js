import { LightningElement } from 'lwc';
import graphic_home from '@salesforce/resourceUrl/graphic_home';

export default class GraphicHome extends LightningElement {
    homeImage = graphic_home;
    isModalOpen = false

    handleModal() {
        this.isModalOpen = !this.isModalOpen;
    }
}
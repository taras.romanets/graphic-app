import { LightningElement, api } from 'lwc';

export default class ModalWindow extends LightningElement {
    @api isModalOpen;
    @api headerText;
    @api bodyText;

    closeModal(){ 
        this.dispatchEvent(new CustomEvent('closemodal', {detail: 'closeModal'}));
    }
}